/* Load modules */
const express = require('express');
const app = express();
const bodyParser = require("body-parser");

/* Database configuration */
const database = require('./app/config/dbconfig');

/* Init database */
database.init();

/* Init server listening */
const port = process.argv[2] || 5000;
app.listen(port, function () {
    console.log("Server listening on port : " + port);
});

/* Express configuration */
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/* Router configuration */
const REST_API_ROOT = '/api';
app.use(REST_API_ROOT, require('./app/routes/router'));

var outDB = {};
outDB.arrId = [];
outDB.arrName = [];
outDB.arrEmail = [];
outDB.arrCity = [];


var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('carona.db', (err) => {
    if (err) {
        console.error(err.message);
    }
    console.log('Connected database.');
});

db.serialize(() => {
    db.each(`SELECT *
           FROM users`, (err, row) => {
        if (err) {
            console.error(err.message);
        }
        outDB.arrId.push(row.id);
        outDB.arrName.push(row.name);
        outDB.arrEmail.push(row.email);
        outDB.arrCity.push(row.city);
        console.log(outDB);

    });
});



app.get('/api/db', (req, res) => {
    res.send(outDB);
});