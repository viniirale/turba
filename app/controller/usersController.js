/* Load Users Data Access Object */
const UsersDao = require('../dao/usersDao');

/* Load Controller Common function */
const ControllerCommon = require('./common/controllerCommon');

/* Load Usuario entity */
const Users = require('../model/users');

/**
 * Users Controller
 */
class UsersController {

    constructor() {
        this.usersDao = new UsersDao();
        this.common = new ControllerCommon();
    }

    /**
     * Tries to find an entity using its Id / Primary Key
     * @params req, res
     * @return entity
     */
    findById(req, res) {
        let id = req.params.id;

        this.usersDao.findById(id)
            .then(this.common.findSuccess(res))
            .catch(this.common.findError(res));
    };

    /**
     * Finds all entities.
     * @return all entities
     */
    findAll(res) {
        this.usersDao.findAll()
            .then(this.common.findSuccess(res))
            .catch(this.common.findError(res));
    };

    /**
     * Counts all the records present in the database
     * @return count
     */
    countAll(res) {

        this.usersDao.countAll()
            .then(this.common.findSuccess(res))
            .catch(this.common.serverError(res));
    };

    /**
     * Updates the given entity in the database
     * @params req, res
     * @return true if the entity has been updated, false if not found and not updated
     */
    update(req, res) {
        var reqBody=JSON.parse(Object.keys(req.body));
        let usuario = new Usuario();
        users.id = reqBody.id;
        users.name = reqBody.name;
        users.email = reqBody.email;
        users.city = reqBody.city;

        return this.usersDao.update(users)
            .then(this.common.editSuccess(res))
            .catch(this.common.serverError(res));
    };

    /**
     * Creates the given entity in the database
     * @params req, res
     * returns database insertion status
     */
    create(req, res) {
        var reqBody=JSON.parse(Object.keys(req.body));
        let users = new Users();
        if (req.body.id) {
            users.id = req.body.id;
        }
        users.name = reqBody.name;
        users.email = reqBody.email;
        users.city = reqBody.city;

        if (reqBody.id) {
            return this.usersDao.createWithId(users)
                .then(this.common.editSuccess(res))
                .catch(this.common.serverError(res));
        }
        else {
            return this.usersDao.create(users)
                .then(this.common.editSuccess(res))
                .catch(this.common.serverError(res));
        }

    };

    /**
     * Deletes an entity using its Id / Primary Key
     * @params req, res
     * returns database deletion status
     */
    deleteById(req, res) {
        let id = req.params.id;

        this.usersDao.deleteById(id)
            .then(this.common.editSuccess(res))
            .catch(this.common.serverError(res));
    };

    /**
     * Returns true if an entity exists with the given Id / Primary Key
     * @params req, res
     * @return
     */
    exists(req, res) {
        let id = req.params.id;

        this.usersDao.exists(id)
            .then(this.common.existsSuccess(res))
            .catch(this.common.findError(res));
    };

       
}

module.exports = UsersController;