/**
 * Users Entity (ES6 Class)
 */

class Users {
    constructor(id, name, email, city) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.city = city;
    }
}

module.exports = Users;