/* Load modules */
let sqlite3 = require('sqlite3').verbose();

/*
 * Database configuration
 */

/* Load database file (Creates file if not exists) */
let db = new sqlite3.Database('./carona.db');

/* Init users and driver tables if they don't exist */
let init = function () {
    db.run("CREATE TABLE if not exists users (" +
        "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
        " name TEXT NOT NULL," +
        " email TEXT NOT NULL," +
        " city TEXT" +
        ")");
};
module.exports = {
    init: init,
    db: db
};
