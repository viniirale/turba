/* Load Usuario entity */
const Users = require("../model/users");

/* Load DAO Common functions */
const daoCommon = require("./commons/daoCommon");

/**
 * Usuario Data Access Object
 */
class UsersDao {
  constructor() {
    this.common = new daoCommon();
  }

  /**
   * Tries to find an entity using its Id / Primary Key
   * @params id
   * @return entity
   */
  findById(id) {
    let sqlRequest =
      "SELECT id, name, email, city FROM users WHERE id=$id";
    let sqlParams = { $id: id };
    return this.common
      .findOne(sqlRequest, sqlParams)
      .then(
        row =>
          new Usuario(
            row.id,
            row.name,
            row.email,
            row.city
          )
      );
  }

  /**
   * Finds all entities.
   * @return all entities
   */
  findAll() {
    let sqlRequest = "SELECT * FROM users";
    return this.common.findAll(sqlRequest).then(rows => {
      let users = [];
      for (const row of rows) {
        users.push(
          new Users(
            row.id,
            row.name,
            row.email,
            row.city
          )
        );
      }
      return users;
    });
  }

  /**
   * Counts all the records present in the database
   * @return count
   */
  countAll() {
    let sqlRequest = "SELECT COUNT(*) AS count FROM users";
    return this.common.findOne(sqlRequest);
  }

  /**
   * Updates the given entity in the database
   * @params Users
   * @return true if the entity has been updated, false if not found and not updated
   */
  update(Users) {
    let sqlRequest =
      "UPDATE usuario SET " +
      "name=$name, " +
      "email=$email, " +
      "city=$city " +
      "WHERE id=$id";

    let sqlParams = {
      $name: Users.name,
      $email: Users.email,
      $city: Users.city,
      $id: Users.id
    };
    return this.common.run(sqlRequest, sqlParams);
  }

  /**
   * Creates the given entity in the database
   * @params Users
   * returns database insertion status
   */
  create(Users) {
    let sqlRequest =
      "INSERT into users (name, email, city) " +
      "VALUES ($name, $email, $city)";
    let sqlParams = {
      $name: Users.name,
      $email: Users.email,
      $city: Users.city
    };
    return this.common.run(sqlRequest, sqlParams);
  }

  /**
   * Creates the given entity with a provided id in the database
   * @params Users
   * returns database insertion status
   */
  createWithId(Usuario) {
    let sqlRequest =
      "INSERT into users (id, name, email, city) " +
      "VALUES ($id, $name, $email, $city)";
    let sqlParams = {
      $id: Users.id,
      $name: Users.name,
      $email: Users.email,
      $city: Users.city,
    };
    return this.common.run(sqlRequest, sqlParams);
  }

  /**
   * Deletes an entity using its Id / Primary Key
   * @params id
   * returns database deletion status
   */
  deleteById(id) {
    let sqlRequest = "DELETE FROM users WHERE id=$id";
    let sqlParams = { $id: id };
    return this.common.run(sqlRequest, sqlParams);
  }

  /**
   * Returns true if an entity exists with the given Id / Primary Key
   * @params id
   * returns database entry existence status (true/false)
   */
  exists(id) {
    let sqlRequest = "SELECT COUNT(*) as found FROM users WHERE id=$id";
    let sqlParams = { $id: id };
    return this.common.existsOne(sqlRequest, sqlParams);
  }

  countAll() {
    let sqlRequest = "SELECT COUNT(*) AS count FROM users";
    return this.common.findOne(sqlRequest);
  }
}

module.exports = UsersDao;