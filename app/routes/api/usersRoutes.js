/* Load Modules */
const express = require('express');
const router = express.Router();

/* Load controller */
const UsersController = require('../../controller/usersController');
const usersController = new UsersController();

/**
 * Usuario Entity routes
 */
router.get('/count', function (req, res) {
    usersController.countAll(res);
});

router.get('/exists/:id', function (req, res) {
    usersController.exists(req, res);
});

router.get('/:id', function (req, res) {
    usersController.findById(req, res);
});

router.get('/', function (req, res) {
    usersController.findAll(res);
});

router.put('/:id', function (req, res) {
    usersController.update(req, res);
});

router.post('/create', function (req, res) {
    usersController.create(req, res);
});

router.delete('/:id', function (req, res) {
    usersController.deleteById(req, res);
});

module.exports = router;